class App {
    constructor() {
        // this.clearButton = document.getElementById("clear-btn");
        // this.loadButton = document.getElementById("load-btn");
        this.carContainerElement = document.getElementById("cars-container");
    }

    async init() {
        await this.load();

        // Register click listener
        // this.clearButton.onclick = this.clear;
        // this.loadButton.onclick = this.run;
    }

    run = () => {
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.innerHTML = car.render();
            this.carContainerElement.appendChild(node);
        });
    };

    async load() {
        this.clear();
        // mengambil data dari inputan user dengan menggunakan id
        const passenger = document.getElementById('passenger');
        const checkDate = document.getElementById('date');
        const checkHours = document.getElementById('hours');


        const passengerSeat = passenger.value;
        console.log(passengerSeat)
        const availableDate = checkDate.value;
        const availableHours = checkHours.value;

        // melakukan parse dari date dengan mengambil nilai dari available date dan ditambahkan karakter T yang berfungsi sebagai tanda mulai dari sebuah timestamp ditambahkan dengan nilai dari availablehours dan ditambahkan karakter z sebagai tanda selesai dari timestamp
        let inputDateTime = Date.parse(availableDate + "T" + availableHours + "Z");
        // console.log(inputDateTime);

        const cars = await Binar.listCars();
        // console.log(cars.length)
        const availableCar = cars.filter((car) => {
            // membuat sebuah validasi dengan mereturn kapasitas dari data mobil lebih dari sama dengan integer dari inputan user, data mobil available harus true dan integer dari date parse harus lebih dari input 
            return car.capacity >= parseInt(passengerSeat) && car.available === true && parseInt(Date.parse(car.availableAt)) > inputDateTime;
        })
        Car.init(availableCar);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };
}