class Car {
    static list = [];

    static init(cars) {
        this.list = cars.map((i) => new this(i));
    }

    constructor({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        available,
        type,
        year,
        options,
        specs,
        availableAt,
    }) {
        this.id = id;
        this.plate = plate;
        this.manufacture = manufacture;
        this.model = model;
        this.image = image;
        this.rentPerDay = rentPerDay;
        this.capacity = capacity;
        this.description = description;
        this.transmission = transmission;
        this.available = available;
        this.type = type;
        this.year = year;
        this.options = options;
        this.specs = specs;
        this.availableAt = availableAt;
    }

    // mengembalikan nilai dan menambahkan sebuah class untuk membuat sebuah card yang berisi hasil dari pencarian
    render() {
        return `
        <div class="carContainer align-items-stretch">
          <div class="card p-3">
            <div class="image-card">
              <img src="${this.image}" class="w-100" alt="" style="max-height: 180px;object-fit: cover">
            </div>
  
            <div>
              <p class="fw-bold mt-1">${this.manufacture}/${this.model}</p>
            </div>
            <div>
              <h5 class="fw-bolder"><strong>Rp. ${this.rentPerDay} / hari</strong></h5>
            </div>
            <div>
              <p class = desc>${this.description}</p>
            </div>
            <div>
              <span><img src="./images/fi-users.png" style="width:20px; height:20px"> &ensp;${this.capacity} Orang</span>
            </div>
            <div>
              <span><img src="./images/fi-settings.png"style="width:20px; height:20px"> &ensp;${this.transmission}</span>
            </div>
            <div>
              <span><img src="./images/fi-calendar.png"style="width:20px; height:20px"> &ensp;Tahun ${this.year}</span>
            </div>
            <br>
            <button class="btn btn-success"> Pilih Mobil</button>
          </div>
        </div>
  `;
    }
}